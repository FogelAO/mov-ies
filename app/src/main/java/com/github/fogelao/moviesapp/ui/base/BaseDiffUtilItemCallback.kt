package com.github.fogelao.moviesapp.ui.base

import androidx.recyclerview.widget.DiffUtil
import com.github.fogelao.moviesapp.model.base.ListItem

open class BaseDiffUtilItemCallback : DiffUtil.ItemCallback<ListItem>() {
    override fun areItemsTheSame(oldItem: ListItem, newItem: ListItem) =
        oldItem.itemId == newItem.itemId

    override fun areContentsTheSame(oldItem: ListItem, newItem: ListItem) =
        oldItem.equals(newItem)
}