package com.github.fogelao.moviesapp.ui.main

import android.content.Context
import android.os.Bundle
import android.util.TypedValue
import android.view.View
import androidx.core.view.updatePadding
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import com.github.fogelao.moviesapp.R
import com.github.fogelao.moviesapp.databinding.FragmentMainBinding
import com.github.fogelao.moviesapp.ui.base.viewBinding
import com.github.fogelao.moviesapp.viewmodel.main.MainScreenComponent
import com.github.fogelao.moviesapp.viewmodel.main.MainScreenViewModel
import kotlinx.android.synthetic.main.activity_main.*

class MainFragment : Fragment(R.layout.fragment_main) {
    private val component by lazy { MainScreenComponent.create() }
    private val binding by viewBinding { FragmentMainBinding.bind(it) }
    private val viewModel by viewModels<MainScreenViewModel> { component.viewModelFactory() }

    private val adapter by lazy {
        MainScreenAdapter(
            onItemBind = viewModel::initCategory,
            onReadyToLoadMore = viewModel::readyToLoadMore,
            onClickListener = {
                val action = MainFragmentDirections.actionMainFragmentToMovieDetailsFragment(
                    name = it.title,
                    overview = it.overview,
                    posterUrl = it.posterUrl,
                    backDropUrl = it.backDropUrl
                )
                binding.root.findNavController().navigate(action)
            }
        )
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        with(binding) {
            recyclerView.setOnApplyWindowInsetsListener { view, insets ->
                view.updatePadding(top = insets.systemWindowInsetTop, bottom = insets.systemWindowInsetBottom)
                insets
            }
            recyclerView.adapter = adapter
            recyclerView.itemAnimator = null

            viewModel.data.observe(viewLifecycleOwner, Observer {
                adapter.items = it
            })
        }
    }

    private fun Context.getThemedColor(resId: Int) {
        val typedValue = TypedValue()
        theme.resolveAttribute(resId, typedValue, true)
        val color = typedValue.data
    }
}