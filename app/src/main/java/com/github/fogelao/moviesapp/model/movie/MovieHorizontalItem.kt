package com.github.fogelao.moviesapp.model.movie

import com.github.fogelao.moviesapp.model.base.ListItem
import com.github.fogelao.moviesapp.repository.model.CategoryType

data class MovieHorizontalItem(
    val title: String,
    val category: CategoryType,
    val moves: List<ListItem>
) : ListItem {
    override val itemId: Int = title.hashCode()
}