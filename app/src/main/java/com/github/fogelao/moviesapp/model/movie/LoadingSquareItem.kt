package com.github.fogelao.moviesapp.model.movie

import com.github.fogelao.moviesapp.model.base.ListItem

object LoadingSquareItem : ListItem {
    override val itemId: Int = 0
}