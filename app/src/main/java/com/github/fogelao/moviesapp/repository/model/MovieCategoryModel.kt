package com.github.fogelao.moviesapp.repository.model

import com.github.fogelao.moviesapp.core.network.api.PagingState
import com.github.fogelao.moviesapp.core.network.model.MovieDto

data class MovieCategoryModel(
    val title: String,
    val category: CategoryType,
    val dataState: PagingState<List<MovieDto>>
)