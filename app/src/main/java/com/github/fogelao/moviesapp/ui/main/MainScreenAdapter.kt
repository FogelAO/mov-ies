package com.github.fogelao.moviesapp.ui.main

import com.github.fogelao.moviesapp.model.base.ListItem
import com.github.fogelao.moviesapp.model.movie.MovieHorizontalItem
import com.github.fogelao.moviesapp.model.movie.MovieItem
import com.github.fogelao.moviesapp.repository.model.CategoryType
import com.github.fogelao.moviesapp.ui.base.BaseDiffUtilItemCallback
import com.hannesdorfmann.adapterdelegates4.AsyncListDifferDelegationAdapter

class MainScreenAdapter(
    onItemBind: (MovieHorizontalItem) -> Unit,
    onReadyToLoadMore: (CategoryType, Int) -> Unit,
    onClickListener: (MovieItem) -> Unit
) : AsyncListDifferDelegationAdapter<ListItem>(BaseDiffUtilItemCallback()) {

    init {
        delegatesManager.addDelegate(
            MainScreenDelegates.moviesHorizontalDelegate(
                onItemBind = onItemBind,
                onReadyToLoadMore = onReadyToLoadMore,
                onClickListener = onClickListener
            )
        )
    }
}