package com.github.fogelao.moviesapp.viewmodel.base

import androidx.lifecycle.ViewModel

abstract class BaseViewModel : ViewModel() {
}