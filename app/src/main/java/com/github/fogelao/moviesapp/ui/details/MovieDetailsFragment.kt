package com.github.fogelao.moviesapp.ui.details

import android.os.Bundle
import android.view.View
import androidx.core.view.updatePadding
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.github.fogelao.moviesapp.R
import com.github.fogelao.moviesapp.databinding.FragmentMovieDetailsBinding
import com.github.fogelao.moviesapp.ui.base.viewBinding

class MovieDetailsFragment : Fragment(R.layout.fragment_movie_details) {
    private val binding by viewBinding { FragmentMovieDetailsBinding.bind(it) }

    private val args: MovieDetailsFragmentArgs by navArgs()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val posterUrl = args.posterUrl
        val backdropUrl = args.backDropUrl

        binding.name = args.name
        binding.overView = args.overview

        Glide.with(binding.root)
            .load(posterUrl)
            .override(
                resources.getDimensionPixelOffset(R.dimen.movie_poster_width),
                resources.getDimensionPixelOffset(R.dimen.movie_poster_height)
            )
            .transform(CenterCrop())
            .transition(DrawableTransitionOptions.withCrossFade())
            .into(binding.posterImageView)

        Glide.with(binding.root)
            .load(backdropUrl)
            .transform(CenterCrop())
            .transition(DrawableTransitionOptions.withCrossFade())
            .into(binding.backgroundImageView)
    }
}