package com.github.fogelao.moviesapp.model.base

interface ListItem {
    val itemId: Int
}