package com.github.fogelao.moviesapp.model.movie

import com.github.fogelao.moviesapp.model.base.ListItem

sealed class MovieItem(
    val id: Int,
    val title: String,
    val overview: String,
    val releaseDate: String,
    val posterUrl: String?,
    val backDropUrl: String?
) : ListItem {

    class MovieThinItem(
        id: Int,
        title: String,
        overview: String,
        releaseDate: String,
        imageUrl: String?,
        backDropUrl: String?
    ) : MovieItem(id, title, overview, releaseDate, imageUrl, backDropUrl) {
        override val itemId: Int = id
    }

    class MovieWideItem(
        id: Int,
        title: String,
        overview: String,
        releaseDate: String,
        imageUrl: String?,
        backDropUrl: String?
    ) : MovieItem(id, title, overview, releaseDate, imageUrl, backDropUrl) {
        override val itemId: Int = id
    }
}