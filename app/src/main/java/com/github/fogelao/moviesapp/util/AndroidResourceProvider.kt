package com.github.fogelao.moviesapp.util

import android.content.Context
import javax.inject.Inject

class AndroidResourceProvider @Inject constructor(
    private val context: Context
) : ResourceProvider {
    override fun getString(id: Int) = context.getString(id)
}