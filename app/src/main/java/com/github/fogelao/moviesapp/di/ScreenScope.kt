package com.github.fogelao.moviesapp.di

import javax.inject.Scope

@Scope
annotation class ScreenScope