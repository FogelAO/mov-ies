package com.github.fogelao.moviesapp.ui.main

import android.app.Activity
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.github.fogelao.moviesapp.R
import com.github.fogelao.moviesapp.databinding.*
import com.github.fogelao.moviesapp.model.base.ListItem
import com.github.fogelao.moviesapp.model.movie.*
import com.github.fogelao.moviesapp.repository.model.CategoryType
import com.hannesdorfmann.adapterdelegates4.dsl.adapterDelegateViewBinding
import timber.log.Timber

object MainScreenDelegates {

    fun moviesHorizontalDelegate(
        onItemBind: (MovieHorizontalItem) -> Unit,
        onReadyToLoadMore: (CategoryType, Int) -> Unit,
        onClickListener: (MovieItem) -> Unit
    ) =
        adapterDelegateViewBinding<MovieHorizontalItem, ListItem, ItemMoviesHorizontalBinding>(
            { inflater, container ->
                ItemMoviesHorizontalBinding.inflate(
                    inflater,
                    container,
                    false
                )
            }
        ) {
            val adapter =
                MovieCardsAdapter(
                    { position -> onReadyToLoadMore.invoke(item.category, position) },
                    onClickListener
                )
            binding.recyclerView.adapter = adapter

            bind {
                onItemBind.invoke(item)
                binding.title = item.title
                adapter.items = item.moves
            }
        }

    fun loadingWideDelegate() =
        adapterDelegateViewBinding<LoadingWideItem, ListItem, ItemLoadingWideBinding>(
            { inflater, container -> ItemLoadingWideBinding.inflate(inflater, container, false) }
        ) {}

    fun movieWideDelegate(
        onReadyToLoadMore: (Int) -> Unit,
        onClickListener: (MovieItem) -> Unit
    ) =
        adapterDelegateViewBinding<MovieItem.MovieWideItem, ListItem, ItemMovieWideBinding>(
            { inflater, container -> ItemMovieWideBinding.inflate(inflater, container, false) }
        ) {
            binding.root.setOnClickListener {
                onClickListener.invoke(item)
            }

            bind {
                val resources = binding.root.resources

                Glide.with(binding.root)
                    .load(item.posterUrl)
                    .override(
                        resources.getDimensionPixelOffset(R.dimen.movie_card_wide_width),
                        resources.getDimensionPixelOffset(R.dimen.movie_card_wide_height)
                    )
                    .transform(
                        CenterCrop(),
                        RoundedCorners(resources.getDimensionPixelOffset(R.dimen.movie_card_radius))
                    )
                    .transition(withCrossFade())
                    .into(binding.imageView)

                binding.title = item.title
                binding.release = item.releaseDate
                binding.executePendingBindings()

                Timber.e("Bind $adapterPosition")

                onReadyToLoadMore.invoke(adapterPosition)
            }

            onViewRecycled {
                if ((binding.root.context as? Activity)?.isDestroyed?.not() == true)
                    Glide.with(binding.root).clear(binding.imageView)
            }
        }

    fun loadingThinDelegate() =
        adapterDelegateViewBinding<LoadingSquareItem, ListItem, ItemLoadingThinBinding>(
            { inflater, container -> ItemLoadingThinBinding.inflate(inflater, container, false) }
        ) {}

    fun movieThinDelegate(
        onReadyToLoadMore: (Int) -> Unit,
        onClickListener: (MovieItem) -> Unit
    ) =
        adapterDelegateViewBinding<MovieItem.MovieThinItem, ListItem, ItemMovieThinBinding>(
            { inflater, container -> ItemMovieThinBinding.inflate(inflater, container, false) }
        ) {

            binding.root.setOnClickListener {
                onClickListener.invoke(item)
            }

            bind {
                val resources = binding.root.resources

                Glide.with(binding.root)
                    .load(item.posterUrl)
                    .override(
                        resources.getDimensionPixelOffset(R.dimen.movie_card_thin_width),
                        resources.getDimensionPixelOffset(R.dimen.movie_card_thin_height)
                    )
                    .transform(
                        CenterCrop(),
                        RoundedCorners(resources.getDimensionPixelOffset(R.dimen.movie_card_radius))
                    )
                    .transition(withCrossFade())
                    .into(binding.imageView)

                binding.title = item.title
                binding.release = item.releaseDate
                binding.executePendingBindings()

                onReadyToLoadMore.invoke(adapterPosition)
            }

            onViewRecycled {
                if ((binding.root.context as? Activity)?.isDestroyed?.not() == true)
                    Glide.with(binding.root).clear(binding.imageView)
            }
        }
}