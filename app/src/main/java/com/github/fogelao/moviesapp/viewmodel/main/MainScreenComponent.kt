package com.github.fogelao.moviesapp.viewmodel.main

import androidx.lifecycle.ViewModel
import com.github.fogelao.moviesapp.DI
import com.github.fogelao.moviesapp.core.network.api.MoviesApi
import com.github.fogelao.moviesapp.di.ScreenScope
import com.github.fogelao.moviesapp.di.ViewModelFactory
import com.github.fogelao.moviesapp.di.ViewModelKey
import com.github.fogelao.moviesapp.interactor.main.MainScreenInteractor
import com.github.fogelao.moviesapp.interactor.main.MainScreenInteractorImpl
import com.github.fogelao.moviesapp.util.ResourceProvider
import dagger.Binds
import dagger.BindsInstance
import dagger.Component
import dagger.Module
import dagger.multibindings.IntoMap

@Component(modules = [MainScreenModule::class])
@ScreenScope
interface MainScreenComponent {

    fun viewModelFactory(): ViewModelFactory

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun resources(resources: ResourceProvider): Builder

        @BindsInstance
        fun api(api: MoviesApi): Builder

        fun build(): MainScreenComponent
    }

    companion object {
        fun create() = with(DI.appComponent) {
            DaggerMainScreenComponent.builder()
                .resources(resources())
                .api(DI.networkComponent.moviesApi())
                .build()
        }
    }
}

@Module
abstract class MainScreenModule {

    @Binds
    @IntoMap
    @ViewModelKey(MainScreenViewModel::class)
    abstract fun mainScreenViewModel(viewModel: MainScreenViewModel): ViewModel

    @Binds
    @ScreenScope
    abstract fun mainScreenInteractor(interactor: MainScreenInteractorImpl): MainScreenInteractor

}