package com.github.fogelao.moviesapp

import android.app.Application
import android.content.Context
import com.github.fogelao.moviesapp.core.network.di.DaggerNetworkComponent
import com.github.fogelao.moviesapp.di.DaggerAppComponent
import com.jakewharton.threetenabp.AndroidThreeTen
import timber.log.Timber

class App : Application() {

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
    }

    override fun onCreate() {
        super.onCreate()

        initDI()
        Timber.plant(Timber.DebugTree())
        AndroidThreeTen.init(this)
    }

    private fun initDI() {
        DI.appComponent = DaggerAppComponent.builder()
            .appContext(this)
            .build()

        DI.networkComponent = DaggerNetworkComponent.create()
    }
}