package com.github.fogelao.moviesapp.interactor.main

import com.github.fogelao.moviesapp.model.base.ListItem
import com.github.fogelao.moviesapp.repository.model.CategoryType
import kotlinx.coroutines.flow.Flow

interface MainScreenInteractor {

    fun data(): Flow<List<ListItem>>

    suspend fun initCategory(categoryType: CategoryType)

    suspend fun tryToLoadMore(categoryType: CategoryType, index: Int)
}