package com.github.fogelao.moviesapp.ui.main

import com.github.fogelao.moviesapp.model.base.ListItem
import com.github.fogelao.moviesapp.model.movie.MovieItem
import com.github.fogelao.moviesapp.ui.base.BaseDiffUtilItemCallback
import com.hannesdorfmann.adapterdelegates4.AsyncListDifferDelegationAdapter

class MovieCardsAdapter(
    onReadyToLoadMore: (Int) -> Unit,
    onClickListener: (MovieItem) -> Unit
) : AsyncListDifferDelegationAdapter<ListItem>(BaseDiffUtilItemCallback()) {
    init {
        delegatesManager.run {
            addDelegate(MainScreenDelegates.movieWideDelegate(onReadyToLoadMore, onClickListener))
            addDelegate(MainScreenDelegates.movieThinDelegate(onReadyToLoadMore, onClickListener))
            addDelegate(MainScreenDelegates.loadingWideDelegate())
            addDelegate(MainScreenDelegates.loadingThinDelegate())
        }
    }
}