package com.github.fogelao.moviesapp

import com.github.fogelao.moviesapp.core.network.di.NetworkComponent
import com.github.fogelao.moviesapp.di.AppComponent

object DI {
    lateinit var appComponent: AppComponent
        internal set

    lateinit var networkComponent: NetworkComponent
        internal set
}