package com.github.fogelao.moviesapp.repository

import com.github.fogelao.moviesapp.R
import com.github.fogelao.moviesapp.core.network.api.MoviesRemoteDataSource
import com.github.fogelao.moviesapp.core.network.api.params.RequestType
import com.github.fogelao.moviesapp.core.network.api.params.MoviesApiParams
import com.github.fogelao.moviesapp.repository.model.CategoryType
import com.github.fogelao.moviesapp.repository.model.MovieCategoryModel
import com.github.fogelao.moviesapp.util.ResourceProvider
import kotlinx.coroutines.flow.map
import javax.inject.Inject


class UpcomingMoviesRepositoryImpl @Inject constructor(
    private val dataSource: MoviesRemoteDataSource,
    private val resources: ResourceProvider
) : MoviesCategoryRepository {

    init {
        dataSource.type = RequestType.UPCOMING
    }

    override fun data() = dataSource.observe()
        .map {
            MovieCategoryModel(
                title = resources.getString(R.string.upcoming),
                category = CategoryType.Upcoming,
                dataState = it
            )
        }

    override suspend fun init() {
        dataSource.initialLoading(
            MoviesApiParams()
        )
    }

    override suspend fun loadMore(index: Int) {
        dataSource.loadMore(index)
    }
}