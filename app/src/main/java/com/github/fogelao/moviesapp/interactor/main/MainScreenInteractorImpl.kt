package com.github.fogelao.moviesapp.interactor.main

import com.github.fogelao.moviesapp.core.network.api.MoviesApi
import com.github.fogelao.moviesapp.core.network.api.MoviesRemoteDataSource
import com.github.fogelao.moviesapp.core.network.api.PagingState
import com.github.fogelao.moviesapp.core.network.model.MovieDto
import com.github.fogelao.moviesapp.model.base.ListItem
import com.github.fogelao.moviesapp.model.movie.*
import com.github.fogelao.moviesapp.repository.PopularMoviesRepositoryImpl
import com.github.fogelao.moviesapp.repository.TrendingMoviesRepositoryImpl
import com.github.fogelao.moviesapp.repository.UpcomingMoviesRepositoryImpl
import com.github.fogelao.moviesapp.repository.model.CategoryType
import com.github.fogelao.moviesapp.repository.model.MovieCategoryModel
import com.github.fogelao.moviesapp.util.ResourceProvider
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.transform
import org.threeten.bp.LocalDate
import org.threeten.bp.format.DateTimeFormatter
import javax.inject.Inject

class MainScreenInteractorImpl @Inject constructor(
    api: MoviesApi,
    resources: ResourceProvider
) : MainScreenInteractor {

    private val popularCategoryRepository = PopularMoviesRepositoryImpl(
        MoviesRemoteDataSource(api), resources
    )
    private val upcomingCategoryRepository = UpcomingMoviesRepositoryImpl(
        MoviesRemoteDataSource(api), resources
    )
    private val trendingCategoryRepository = TrendingMoviesRepositoryImpl(
        MoviesRemoteDataSource(api), resources
    )

    override fun data(): Flow<List<ListItem>> = combine(
        popularCategoryRepository.data(),
        upcomingCategoryRepository.data(),
        trendingCategoryRepository.data()
    ) { popular, upcoming, trending ->
        listOf(
            mapToCategory(popular, true),
            mapToCategory(upcoming),
            mapToCategory(trending, true)
        )
    }

    override suspend fun initCategory(categoryType: CategoryType) {
        when (categoryType) {
            is CategoryType.Popular -> popularCategoryRepository.init()
            is CategoryType.Upcoming -> upcomingCategoryRepository.init()
            is CategoryType.Trending -> trendingCategoryRepository.init()
        }
    }

    override suspend fun tryToLoadMore(categoryType: CategoryType, index: Int) {
        when (categoryType) {
            is CategoryType.Popular -> popularCategoryRepository.loadMore(index)
            is CategoryType.Upcoming -> upcomingCategoryRepository.loadMore(index)
            is CategoryType.Trending -> trendingCategoryRepository.loadMore(index)
        }
    }

    private fun mapToCategory(model: MovieCategoryModel, wide: Boolean = false) =
        when (model.dataState) {
            is PagingState.Initial -> {
                MovieHorizontalItem(
                    title = model.title,
                    category = model.category,
                    moves = IntRange(1, 4).map { if (wide) LoadingWideItem else LoadingSquareItem }
                )
            }

            is PagingState.Content -> {
                MovieHorizontalItem(
                    title = model.title,
                    category = model.category,
                    moves = model.dataState.data.map { movie -> mapMovieItem(movie, wide) }
                )
            }

            is PagingState.Paging -> {
                MovieHorizontalItem(
                    title = model.title,
                    category = model.category,
                    moves = model.dataState.availableContent.map { movie ->
                        mapMovieItem(
                            movie,
                            wide
                        )
                    }
                        .plus(if (wide) LoadingWideItem else LoadingSquareItem)
                )
            }

            else -> throw IllegalStateException("Wrong paging state: ${model.dataState}")
        }

    private fun mapMovieItem(movie: MovieDto, wide: Boolean = false) = if (wide)
        MovieItem.MovieWideItem(
            id = movie.id,
            title = movie.title,
            overview = movie.overview,
            releaseDate = getDate(movie.releaseDate),
            imageUrl = getImageUrl(movie.posterPath),
            backDropUrl = getImageUrl(movie.backDropPath)
        )
    else
        MovieItem.MovieThinItem(
            id = movie.id,
            title = movie.title,
            overview = movie.overview,
            releaseDate = getDate(movie.releaseDate),
            imageUrl = getImageUrl(movie.posterPath),
            backDropUrl = getImageUrl(movie.backDropPath)
        )

    private fun getImageUrl(path: String?, size: Int = 780) =
        "https://image.tmdb.org/t/p/w$size/$path"

    private fun getDate(date: LocalDate?): String {
        if (date == null) return ""

        return date.format(DateTimeFormatter.ofPattern("d MMM, yyyy"))
    }
}