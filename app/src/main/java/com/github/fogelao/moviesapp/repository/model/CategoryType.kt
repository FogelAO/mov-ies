package com.github.fogelao.moviesapp.repository.model

sealed class CategoryType {
    object Popular : CategoryType()
    object Upcoming : CategoryType()
    object Trending : CategoryType()

    data class Another(val id: Int) : CategoryType()
}