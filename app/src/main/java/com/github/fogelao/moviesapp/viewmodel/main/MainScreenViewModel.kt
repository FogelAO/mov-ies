package com.github.fogelao.moviesapp.viewmodel.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.github.fogelao.moviesapp.interactor.main.MainScreenInteractor
import com.github.fogelao.moviesapp.model.base.ListItem
import com.github.fogelao.moviesapp.model.movie.MovieHorizontalItem
import com.github.fogelao.moviesapp.model.movie.MovieItem
import com.github.fogelao.moviesapp.repository.model.CategoryType
import com.github.fogelao.moviesapp.viewmodel.base.BaseViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

class MainScreenViewModel @Inject constructor(
    private val interactor: MainScreenInteractor
) : BaseViewModel() {

    private val _data = MutableLiveData<List<ListItem>>()
    val data: LiveData<List<ListItem>> = _data

    init {
        viewModelScope.launch(Dispatchers.IO) {
            interactor.data().collect {
                _data.postValue(it)
            }
        }
    }

    fun initCategory(item: MovieHorizontalItem) {
        viewModelScope.launch(Dispatchers.IO) {
            interactor.initCategory(item.category)
        }
    }

    fun readyToLoadMore(category: CategoryType, index: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            interactor.tryToLoadMore(category, index)
        }
    }
}