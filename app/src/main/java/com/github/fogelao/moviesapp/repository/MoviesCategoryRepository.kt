package com.github.fogelao.moviesapp.repository

import com.github.fogelao.moviesapp.repository.model.MovieCategoryModel
import kotlinx.coroutines.flow.Flow

interface MoviesCategoryRepository {

    fun data(): Flow<MovieCategoryModel>

    suspend fun init()

    suspend fun loadMore(index:Int)
}