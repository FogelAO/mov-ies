package com.github.fogelao.moviesapp.core.network.api

import com.github.fogelao.moviesapp.core.network.model.base.PagedResponse
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.QueryMap

interface MoviesApi {

    @GET("/3/movie/upcoming?api_key=2825e61dbb708b7899d75be1fe55cc9b")
    suspend fun upcoming(@QueryMap params: Map<String, String>): PagedResponse

    @GET("/3/trending/movie/week?api_key=2825e61dbb708b7899d75be1fe55cc9b")
    suspend fun trendingWeek(@QueryMap params: Map<String, String>): PagedResponse

    @GET("3/movie/popular?api_key=2825e61dbb708b7899d75be1fe55cc9b")
    suspend fun popular(@QueryMap params: Map<String, String>): PagedResponse

    @GET("3/movie/{type}?api_key=2825e61dbb708b7899d75be1fe55cc9b")
    suspend fun movies(
        @Path("type") type: String,
        @QueryMap params: Map<String, String>
    ): PagedResponse
}