package com.github.fogelao.moviesapp.core.network.model.base

import com.github.fogelao.moviesapp.core.network.model.MovieDto
import com.google.gson.annotations.SerializedName

data class PagedResponse(
    @SerializedName("page") val page: Int,
    @SerializedName("total_results") val totalResults: Int,
    @SerializedName("total_pages") val totalPages: Int,
    @SerializedName("results") val results: List<MovieDto>
)