package com.github.fogelao.moviesapp.core.network.common

import com.google.gson.*
import org.threeten.bp.LocalDate
import org.threeten.bp.format.DateTimeFormatter
import java.lang.reflect.Type

internal class LocalDateConverter :
    JsonSerializer<LocalDate>,
    JsonDeserializer<LocalDate> {
    private val formatter = DateTimeFormatter.ISO_DATE

    override fun serialize(
        src: LocalDate,
        typeOfSrc: Type,
        context: JsonSerializationContext
    ) = JsonPrimitive(formatter.format(src))

    override fun deserialize(
        json: JsonElement,
        typeOfT: Type,
        context: JsonDeserializationContext
    ): LocalDate = formatter.parse(json.asString, LocalDate::from)

}