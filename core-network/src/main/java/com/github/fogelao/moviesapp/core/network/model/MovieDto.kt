package com.github.fogelao.moviesapp.core.network.model

import com.google.gson.annotations.SerializedName
import org.threeten.bp.LocalDate

data class MovieDto(
    @SerializedName("id") val id: Int,
    @SerializedName("title") val title: String,
    @SerializedName("overview") val overview: String,
    @SerializedName("release_date") val releaseDate: LocalDate?,
    @SerializedName("poster_path") val posterPath: String?,
    @SerializedName("backdrop_path") val backDropPath: String?
)