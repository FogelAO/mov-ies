package com.github.fogelao.moviesapp.core.network.api

import com.github.fogelao.moviesapp.core.network.api.params.MoviesApiParams
import com.github.fogelao.moviesapp.core.network.api.params.RequestType
import com.github.fogelao.moviesapp.core.network.model.MovieDto
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.channels.ConflatedBroadcastChannel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.asFlow
import javax.inject.Inject

class MoviesRemoteDataSource @Inject constructor(
    private val api: MoviesApi
) {
    //TODO: Move it into constructor
    var type = RequestType.POPULAR

    private val channel =
        ConflatedBroadcastChannel<PagingState<List<MovieDto>>>(PagingState.Initial)

    private var params: MoviesApiParams? = null
    private var page = 1

    @Synchronized
    suspend fun initialLoading(params: MoviesApiParams) {
        if (channel.value is PagingState.Initial) {
            val response = request(type, params.applyPagingParams())
            this.params = params
            channel.send(PagingState.Content(response.results))
        }
    }

    @Synchronized
    suspend fun loadMore(candidatePosition: Int) {
        val params = this.params ?: return
        val cache = channel.value
        if (cache is PagingState.Content && candidatePosition == cache.data.lastIndex) {
            channel.send(PagingState.Paging(cache.data))
            val response = request(type, params.applyPagingParams(page + 1))
            channel.send(PagingState.Content(cache.data + response.results))
            page += 1
        }
    }

    fun observe(): Flow<PagingState<List<MovieDto>>> = channel.asFlow()

    private fun MoviesApiParams.applyPagingParams(page: Int = 1): Map<String, String> =
        toMap().apply {
            put("page", page.toString())
        }

    private suspend fun request(type: RequestType, params: Map<String, String>) = when (type) {
        RequestType.POPULAR -> api.popular(params)
        RequestType.UPCOMING -> api.upcoming(params)
        RequestType.TRENDING -> api.trendingWeek(params)
    }
}