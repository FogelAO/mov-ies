package com.github.fogelao.moviesapp.core.network.di

import com.github.fogelao.moviesapp.core.network.BuildConfig
import com.github.fogelao.moviesapp.core.network.api.MoviesApi
import com.github.fogelao.moviesapp.core.network.common.LocalDateConverter
import com.google.gson.GsonBuilder
import dagger.Component
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.threeten.bp.LocalDate
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Component(modules = [NetworkModule::class])
@Singleton
interface NetworkComponent {

    fun moviesApi(): MoviesApi
}

@Module
abstract class NetworkModule {


    companion object {
        private const val BASE_URL = "https://api.themoviedb.org/"

        @Provides
        @Singleton
        fun provideApi(): MoviesApi = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(
                OkHttpClient.Builder()
                    .addInterceptor(HttpLoggingInterceptor().apply {
                        level = if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY
                        else HttpLoggingInterceptor.Level.NONE
                    })
                    .build()
            )
            .addConverterFactory(
                GsonConverterFactory.create(
                    GsonBuilder()
                        .registerTypeAdapter(LocalDate::class.java, LocalDateConverter())
                        .create()
                )
            )
            .build()
            .create(MoviesApi::class.java)
    }
}