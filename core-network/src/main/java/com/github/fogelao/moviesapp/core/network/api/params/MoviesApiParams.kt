package com.github.fogelao.moviesapp.core.network.api.params

data class MoviesApiParams(
    val region: String? = null,
    val language: String? = null
) {
    fun toMap(): MutableMap<String, String> = mutableMapOf<String, String>().apply {
        region?.let { put("region", it) }
        language?.let { put("language", it) }
    }
}

enum class RequestType {
    POPULAR,
    UPCOMING,
    TRENDING
}